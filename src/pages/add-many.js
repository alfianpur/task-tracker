import React from 'react'
import dataJson from '../helpers/json/task.json';

const AddMany = () => {

  const handleInsertMany = async () => {
    try {
      const response = await fetch('https://be-vercel-beta.vercel.app/api/add-many-to-do', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ jsonData: dataJson })
      });

      console.log("yares", response)
      
      if (!response.ok) {
        throw new Error('Failed to insert items');
      }
      
      const data = await response.json();
      console.log('Inserted items:', data);
    } catch (error) {
      console.error('Error:', error.message);
    }
  };

  return (
    <div>
      <button onClick={handleInsertMany}>Insert Many Items</button>
    </div>
  );
}

export default AddMany