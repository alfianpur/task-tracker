import React, { useState } from 'react';
import usePostData from '../helpers/hooks/usePostData';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  justify-content: center;
`

const Form = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 550px;

  & > input {
    margin-bottom: 6px;
    padding: 6px; 12px;
    border: none;
  }
`

const AddToDo = () => {
  const [postData, setPostData] = useState({
    title: '',
    due_date: '',
    assign: '',
    status: '',
    priority: '',
    topic: ''
  });
  const { data, loading, error, sendData } = usePostData('https://be-vercel-beta.vercel.app/api/data-to-do', postData);

  const handleChange = event => {
    setPostData({ ...postData, [event.target.name]: event.target.value });
  };

  const handleSubmit = event => {
    event.preventDefault();
    sendData();
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <input
          type="text"
          name="title"
          placeholder="Title"
          value={postData.title}
          onChange={handleChange}
        />
        <input
          type="date"
          name="due_date"
          placeholder="Due Date"
          value={postData.due_date}
          onChange={handleChange}
        />
        <input
          type="text"
          name="assign"
          placeholder="Assign"
          value={postData.assign}
          onChange={handleChange}
        />
        <input
          type="text"
          name="status"
          placeholder="Status"
          value={postData.status}
          onChange={handleChange}
        />
        <input
          type="text"
          name="priority"
          placeholder="Priority"
          value={postData.priority}
          onChange={handleChange}
        />
        <input
          type="text"
          name="topic"
          placeholder="Topic"
          value={postData.topic}
          onChange={handleChange}
        />
        <button type="submit">Submit</button>
      </Form>
      {data && (
        <p>{data.title}</p>
      )}
    </Container>
  );
}

export default AddToDo