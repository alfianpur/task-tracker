import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { bluePrimary, greenPrimary, semiGlass } from '../components/styled-components/constant'
import HomeCard from '../components/card';
import useGetData from '../helpers/hooks/useGetData';

const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding: 80px 0;
`

const Slogan = styled.p`
  font-weight: 600;
  color: ${bluePrimary}
`

const Title = styled.h2`
  font-weight: 600;
  color: white;
  font-size: 36px;
  line-height:42px;
  margin: 32px 0;

  & > span {
    background-image: linear-gradient(135deg, color(display-p3 .6196078431 .4784313725 1 / 1) 0%, color(display-p3 .9960784314 .5450980392 .7333333333 / 1) 33.33%, color(display-p3 1 .7411764706 .4784313725 / 1) 66.67%, color(display-p3 .9725490196 .9176470588 .7647058824 / 1) 100%);
    background-clip: text;
    color: transparent;
  }
`

const Guide = styled.p`
  font-size: 20px;
  margin-bottom: 32px;

  & > a {
    text-decoration: underline;
    font-weight: bold;
    color: ${greenPrimary};
  }
`

const JoinBtn = styled.button`
  padding: 16px 32px;
  background-color: ${bluePrimary};
  font-size: 14px;
  font-weight: bold;
  border: none;
  border-radius: 14px;
  transition: ease 0.5s;

  &:hover {
    transform: scale(1.1)
  }
`

const Dashboard = styled.div`
  width: 100%;
  text-align: center;
  border-top: 1px solid ${semiGlass};
  padding-top: 80px;
`

const DashboardTitle = styled.p`
  margin-bottom: 24px;
`

const CardTracker = styled.div`
  text-align: center;
  padding: 16px;
  border-radius: 16px;
  background: ${semiGlass};
  position: relative;
  width: calc(100% - 24px);
  isolation: isolate;
  overflow: hidden;

  &:before {
    border: 1px solid ${semiGlass};
    content: "";
    left: 0;
    top: 0;
    position: absolute;
    width: calc(100% - 2px);
    height: calc(100% - 2px);
    border-radius: inherit;
    pointer-events: none;
  }
`

const ListWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  text-align: left;
  margin-top: 24px;
`

const Home = () => {

  const { data, loading } = useGetData('https://be-vercel-beta.vercel.app/api/data-to-do');

  return (
    <React.Fragment>
      <Header>
        <Slogan>Boost Your Productivity with Productivity Pal: Where Tasks Meet Triumph!</Slogan>
        <Title>Productivity Pal<br/><span>Your Ultimate Task Tracker ✨</span></Title>
        <Guide>Read our <Link to='/tutorial'>Tutorial</Link> here!</Guide>
        <JoinBtn>Wanna try our feature?</JoinBtn>
      </Header>
      <Dashboard>
        <DashboardTitle>Lets see what you have!</DashboardTitle>
        <CardTracker>
          <p>This Week Task</p>
          <p>Ayo selesaikan tugasmu sebelum minggu ini. Semangat!</p>
          <ListWrapper>
            {loading &&
              <p>Loading</p>
            }
            {data && !loading && data?.returnContent?.map((item, index) => (
              <HomeCard key={index} item={item}/>
            ))}
          </ListWrapper>
        </CardTracker>
      </Dashboard>
    </React.Fragment>
  )
}

export default Home