import React from 'react'
import styled from 'styled-components'
import dataJson from '../helpers/json/task.json';
import PopupModal from '../components/popup-modal';
import { useModal } from '../helpers/context/modal-content';
import useGetData from '../helpers/hooks/useGetData';
import { Link } from 'react-router-dom';
import { convertISOToDate } from '../helpers/date-coverted';

const Container = styled.div`
  display: flex;
  justify-content: center;
  text-align: left;
  padding-top: 48px;
`

const Wrapper = styled.div`

`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;

  & > h2 {
    font-size: 24px;
  }
`

const Table = styled.table`
  border-collapse: collapse;
  border-spacing: 0;
`

const Th = styled.th`
  padding: 10px;
  text-align: left;
  background-color: white;
  color: #080833;
  border-bottom: 1px solid #080833;
`

const Td = styled.td`
  padding: 10px;
  text-align: left;
`

const Tr = styled.tr`
  cursor: pointer;

  &:nth-child(even) {
    background-color: #080833;
  }
`

const Management = () => {
  const { openModal } = useModal();

  const { data, loading, error } = useGetData('https://be-vercel-beta.vercel.app/api/data-to-do');

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <Container>
      <Wrapper>
        <Header>
          <h2>List of To Do</h2>
          <p>Total task: {dataJson.length}</p>
          <Link to='/add-to-do'>Add To Do</Link>
        </Header>
        <Table>
          <thead>
            <tr>
              <Th>Title</Th>
              <Th>Assign</Th>
              <Th>Due Date</Th>
              <Th>Priority</Th>
              <Th>Status</Th>
              <Th>Topic</Th>
            </tr>
          </thead>
          <tbody>
            { data?.returnContent?.map((item, index) => (
              <Tr key={index} onClick={() => openModal(<PopupModal item={item} edit/>)}>
                <Td>{item.title}</Td>
                <Td>{item.assign}</Td>
                <Td>{convertISOToDate(item.due_date)}</Td>
                <Td>{item.priority}</Td>
                <Td>{item.status}</Td>
                <Td>{item.topic}</Td>
              </Tr>
            ))}
            <tr>
            </tr>
          </tbody>
        </Table>
      </Wrapper>
    </Container>
  )
}

export default Management