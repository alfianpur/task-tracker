import { createBrowserRouter } from "react-router-dom";
import MainLayout from "../layouts/main-layout";
import Home from "../pages/home";
import Management from "../pages/management";
import AddToDo from "../pages/add-to-do";
import AddMany from "../pages/add-many";

const mainRoutes = [
  {
    path: "/",
    element: <MainLayout children={<Home/>}/>,
    errorElement: <MainLayout children={<p>Ini error page</p>}/>
  },
  {
    path: "management",
    element: <MainLayout children={<Management/>}/>,
  },
  {
    path: "add-to-do",
    element: <MainLayout children={<AddToDo/>}/>,
  },
  {
    path: "add-many",
    element: <MainLayout children={<AddMany/>}/>,
  },
]

export const router = createBrowserRouter(mainRoutes); 