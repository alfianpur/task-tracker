import React from 'react';
import { useModal } from '../helpers/context/modal-content';
import styled from 'styled-components';

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5); /* Transparansi latar belakang */
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999;
  color: black;
`

const Modal = styled.div`
  background-color: white;
  padding: 20px;
  border-radius: 5px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);

  & > button {
    margin-top: 10px;
    padding: 5px 10px;
    border: none;
    background-color: #007bff;
    color: white;
    cursor: pointer;
  
    &:hover {
      background-color: #0056b3;
    }
  }  
`

const ModalComponent = () => {
  const { isModalOpen, modalContent, closeModal } = useModal();

  return (
    <>
      {isModalOpen && (
        <Overlay>
          <Modal>
            <button onClick={closeModal}>Tutup Modal</button>
            <div>{modalContent}</div>
          </Modal>
        </Overlay>
      )}
    </>
  );
};

export default ModalComponent;
