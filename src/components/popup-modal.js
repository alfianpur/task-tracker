import React from 'react'
import styled from 'styled-components'
import { convertISOToDate } from '../helpers/date-coverted'
import useDelData from '../helpers/hooks/useDelData'
import { useModal } from '../helpers/context/modal-content'

const Container = styled.div`

`

const Title = styled.h2`
  margin: 12px 0;
`

const Table = styled.table`
  font-size: 14px;
`

const DetailContainer = styled.div`
  margin-top: 12px;

  & > p {
    padding-left: 12px;
  }
`

const PopupModal = ({item, edit=false}) => {

  const { closeModal } = useModal();
  
  const { deleteData } = useDelData('https://be-vercel-beta.vercel.app/api/data-to-do', item._id);

  const onDelete = () => {
    deleteData();
    closeModal();
  }
  
  return (
    <Container>
      <Title>{item.title}</Title>
      <Table>
        <tbody>
          <tr>
            <th>Assign</th>
            <td>: {item.assign}</td>
          </tr>
          <tr>
            <th>Priority</th>
            <td>: {item.priority}</td>
          </tr>
          <tr>
            <th>Label</th>
            <td>: {item.status}</td>
          </tr>
          <tr>
            <th>Topic</th>
            <td>: {item.topic}</td>
          </tr>
          <tr>
            <th>Date</th>
            <td>: {convertISOToDate(item.due_date)}</td>
          </tr>
        </tbody>
      </Table>
      <DetailContainer>
        <h2>Detail</h2>
        <p>Ini detailnya</p>
      </DetailContainer>
      { edit &&
        <DetailContainer>
          <h2>Action</h2>
          <button onClick={onDelete}>Delete</button>
        </DetailContainer>
      }
    </Container>
  )  
}

export default PopupModal