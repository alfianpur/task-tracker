import React from 'react'
import { ReactComponent as ImportantA } from '../icons/imp3.svg';
import { ReactComponent as ImportantB } from '../icons/imp2.svg';
import { ReactComponent as ImportantC } from '../icons/imp1.svg';
import styled from 'styled-components';

const ImportantALogo = styled(ImportantA)`
  width: ${props => props.size || '34px'};
  aspect-ratio: 1;
`

const ImportantBLogo = styled(ImportantB)`
  width: ${props => props.size || '34px'};
  aspect-ratio: 1;
`

const ImportantCLogo = styled(ImportantC)`
  width: ${props => props.size || '34px'};
  aspect-ratio: 1;
`

const Priority = ({level = 'a'}) => {

  switch (level) {
    case 'High':
      return (
        <ImportantALogo/>
      )
    case 'Medium':
      return (
        <ImportantBLogo/>
      )
    case 'Low':
      return (
        <ImportantCLogo/>
      )
    default:
      return (
        <></>
      )
  }
}

export default Priority