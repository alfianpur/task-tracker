import React from 'react'
import { ReactComponent as Done } from '../icons/done.svg';
import { ReactComponent as Pending } from '../icons/pending.svg';
import styled from 'styled-components';

const DoneLogo = styled(Done)`
  width: ${props => props.size || '34px'};
  aspect-ratio: 1;
  margin-left: 4px;
`

const PendingLogo = styled(Pending)`
  width: ${props => props.size || '34px'};
  aspect-ratio: 1;
  margin-left: 4px;
`

const TagSwitch = ({tag = 'a'}) => {

  switch (tag) {
    case 'Done':
      return (
        <DoneLogo/>
      )
    case 'Pending':
      return (
        <PendingLogo/>
      )
    default:
      return (
        <></>
      )
  }
}

export default TagSwitch