import React from 'react'
import styled from 'styled-components'
import { semiGlass } from './styled-components/constant'
import { useModal } from '../helpers/context/modal-content'
import PopupModal from './popup-modal';
import Priority from './icon-render/priority';
import TagSwitch from './icon-render/tag';
import { convertISOToDate } from '../helpers/date-coverted';

const Container = styled.div`
  margin: 4px;
  padding: 26px 14px;
  background-color: ${semiGlass};
  width: 200px;
  aspect-ratio: 1;
  border-radius: 12px;
  font-size: 14px;

  display: flex;
  flex-direction: column;

  cursor: pointer;
`

const Date = styled.p`
  margin-bottom: 12px;
  opacity: .75;
`

const Title = styled.p`
  margin-bottom: 12px;
`

const Topic = styled.p`
  padding: 4px;
  background-color: purple;
  text-transform: uppercase;
  border-radius: 8px;
  width: fit-content;
  font-weight: 600;
  font-size: 14px;
  margin: auto 0 14px;
`

const Bottom = styled.div`
  display: flex;
  align-items: center;
`

const BottomRight = styled.div`
  margin-left: auto;
  padding: 12px;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  background-color: black;

  display: flex;
  justify-content: center;
  align-items: center;
`

const HomeCard = ({index, item}) => {

  const { openModal } = useModal();

  return (
    <Container key={index} onClick={() => openModal(<PopupModal item={item}/>)}>
      <Date>{convertISOToDate(item.due_date)}</Date>
      <Title>{item.title}</Title>
      <Topic>{item.topic}</Topic>
      <Bottom>
        <Priority level={item.priority}/>
        <TagSwitch tag={item.status}/>
        <BottomRight>
          <p>{item.assign.slice(0,1)}</p>
        </BottomRight>
      </Bottom>
    </Container>
  )
}

export default HomeCard