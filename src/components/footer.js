import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components';

const FooterWrapper = styled.footer`
  margin-top: auto;
  padding: 24px 0;
`

const Anchor = styled(Link)`
  color: white !important;
  display: block;
  font-size: 0.8rem;
  text-align: center;
  text-decoration: none;
  line-height: 1.5;
`

const Footer = () => {
  return (
    <FooterWrapper>
      <Anchor to="https://www.builder.io/" target="_blank">
        <span>Made with ♡ by Alfian</span>
      </Anchor>
    </FooterWrapper>
  )
}

export default Footer