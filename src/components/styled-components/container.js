import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 24px;
`

export const MainWrapper = styled.div`
  width: 100%;
  max-width: 1440px;
  min-height: 100vh;

  display: flex;
  flex-direction: column;

`

export const WrapperContainer = ({children}) => {
  return (
    <MainContainer>
      <MainWrapper>
        {children}
      </MainWrapper>
    </MainContainer>
  )
}