import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Container = styled.nav`
  display: flex;
  width: 100%;
  margin-top: 24px;
`

const Title = styled(Link)`
  font-weight: bold;
`

const ListMenu = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`

const To = styled(Link)`
  margin-left: 12px;
  font-size: 14px;
`

const Navigation = () => {
  return (
    <Container>
      <Title to='/'>POC Qwik and React</Title>
      <ListMenu>
        <To to='/'>Home</To>
        <To to='/management'>Management</To>
      </ListMenu>
    </Container>
  )
}

export default Navigation