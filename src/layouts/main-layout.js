import React from 'react'
import Footer from '../components/footer'
import Navigation from '../components/navigation'
import { WrapperContainer } from '../components/styled-components/container'

const MainLayout = ({children}) => {
  return (
    <WrapperContainer>
      <Navigation/>
      {children}
      <Footer/>
    </WrapperContainer>
  )
}

export default MainLayout