import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from "react-router-dom";
import './index.css';
import './reset.css';
import { router } from './helpers/routes';
import { ModalProvider } from './helpers/context/modal-content';
import ModalComponent from './components/modal';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ModalProvider>
      <ModalComponent/>
      <RouterProvider router={router} />
    </ModalProvider>
  </React.StrictMode>
);